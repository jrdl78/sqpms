package de.thm.sqpms.data;

public class DataSet {

    protected static class Row{

        public final String[] data;

        public Row(String[] data){
            this.data = data;
        }
    }
}
