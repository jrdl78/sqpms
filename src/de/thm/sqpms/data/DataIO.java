package de.thm.sqpms.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataIO implements IDataSource {

    private final String CSV_SEPARATOR = ";";

    public static IDataSource createNewInstance(String[] csvSources){
        return new DataIO(csvSources);
    }

    private DataIO(String[] csvSources){

    }

    private void loadFiles(File[] files){
        for(File file : files){
            loadFile(file);
        }
    }

    private void loadFile(File file){

        String[] header;
        List<DataSet.Row> rows = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(file))){

            String line = reader.readLine();
            if(line == null) throw new IOException("Unvalid csv format");

            header = line.split(CSV_SEPARATOR);

            while((line = reader.readLine()) != null){

                String[] data = line.split(CSV_SEPARATOR);

                if(data.length != header.length){
                    continue;
                }
                rows.add(new DataSet.Row(data));
            }
            System.out.println("loaded");
        } catch(IOException e){

        }

    }
}
